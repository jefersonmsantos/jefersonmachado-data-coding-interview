import psycopg2
import pandas as pd
from sqlalchemy import create_engine, text

# Define the database connection parameters
db_params = {
    'host': 'localhost',
    'database': 'dw_flights',
    'user': 'postgres',
    'password': 'Password1234**'
}

engine = create_engine(f'postgresql://{db_params["user"]}:{db_params["password"]}@{db_params["host"]}/{db_params["database"]}')

csv_files = {
    'nyc_airlines': './dataset/nyc_airlines.csv',
    'nyc_airports': './dataset/nyc_airports.csv',
    'nyc_flights': './dataset/nyc_flights.csv',
    'nyc_planes': './dataset/nyc_planes.csv',
    'nyc_weather': './dataset/nyc_weather.csv',
}

for table_name, file_path in csv_files.items():
    df = pd.read_csv(file_path)

    df.to_sql(table_name, engine, if_exists='replace', index=False, schema='jms__sources')
