{{
    config(
        alias="flights_summary",
        materialization="table"
    )

}}


with processed_flights as (
	select 
		flights.*
		, left(cast(dep_time as varchar), length(cast(dep_time as varchar))-2)  as dep_hour
		, left(cast(sched_dep_time as varchar), length(cast(sched_dep_time as varchar))-2) as sched_dep_hour
		, left(cast(arr_time as varchar), length(cast(arr_time as varchar))-2) as arr_hour
		, left(cast(sched_arr_time as varchar), length(cast(sched_arr_time as varchar))-2)  as sched_arr_hour


	from {{source('flight_data','nyc_flights')}} as flights

)



select flights.flight as flight
	,  date(concat(flights.year, '-',flights.month, '-',flights.day)) as flight_date
    , flights.tailnum as tailnum
    , flights.sched_dep_time  as scheduled_departure_time
    , flights.dep_time  as actual_departure_time
    , flights.dep_delay as departure_delay
    , flights.sched_arr_time  as scheduled_arrival_time
    , flights.arr_time  as actual_arrival_time
    , flights.arr_delay as arrival_delay
    , flights.origin as origin_faa
    , airports.name as origin_name
    , airports.lat as origin_latitude
    , airports.lon as origin_longitude
    , airports.alt as origin_alttude
    , weather.temp as origin_temp
from
processed_flights as flights
left join {{source('flight_data','nyc_airports')}} as airports
 on flights.origin = airports.faa
left join {{source('flight_data','nyc_weather')}} as weather
 on flights.origin = weather.origin 
  and flights.year = weather.year
  and flights.month = weather.month
  and flights.day = weather.day
  and flights.dep_hour = cast(weather.hour as varchar)
  

select 
    flights.flight as flight
    , flights.tailnum as tailnum
from
{{source('flight_data','nyc_flights')}} as flights

